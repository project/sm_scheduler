<?php

declare(strict_types = 1);

namespace Drupal\sm_scheduler_test;

/**
 * @see \Drupal\sm_scheduler_test\Messenger\ScheduledTestMessageHandler
 */
final class ScheduledTestMessage {

  /**
   * Creates a new ScheduledTestMessage.
   */
  public function __construct(
    public ?string $handledBy = NULL,
  ) {
  }

}
