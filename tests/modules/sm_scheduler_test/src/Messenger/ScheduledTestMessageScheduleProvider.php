<?php

declare(strict_types = 1);

namespace Drupal\sm_scheduler_test\Messenger;

use Drupal\sm_scheduler_test\ScheduledTestMessage;
use Symfony\Component\Scheduler\Attribute\AsSchedule;
use Symfony\Component\Scheduler\RecurringMessage;
use Symfony\Component\Scheduler\Schedule;
use Symfony\Component\Scheduler\ScheduleProviderInterface;

/**
 * Generates messages at an interval thanks to Symfony Scheduler.
 *
 * The derived transport ID is `scheduler_scheduler_test`.
 */
#[AsSchedule('scheduler_test')]
final class ScheduledTestMessageScheduleProvider implements ScheduleProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function getSchedule(): Schedule {
    return (new Schedule())->add(
      RecurringMessage::every('2 seconds', new ScheduledTestMessage()),
    );
  }

}
