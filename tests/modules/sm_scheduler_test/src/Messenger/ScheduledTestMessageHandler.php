<?php

declare(strict_types = 1);

namespace Drupal\sm_scheduler_test\Messenger;

use Drupal\sm_scheduler_test\ScheduledTestMessage;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

/**
 * Message handler for ScheduledTestMessage.
 *
 * @see \Drupal\sm_scheduler_test\ScheduledTestMessage
 */
#[AsMessageHandler]
final class ScheduledTestMessageHandler {

  /**
   * Message handler for ScheduledTestMessage messages.
   */
  public function __invoke(ScheduledTestMessage $message): void {
    $message->handledBy = __METHOD__;
  }

}
