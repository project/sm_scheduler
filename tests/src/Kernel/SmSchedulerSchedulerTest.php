<?php

declare(strict_types = 1);

namespace Drupal\Tests\sm_scheduler\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\KernelTests\KernelTestBase;
use Drupal\sm_scheduler_test\Messenger\ScheduledTestMessageHandler;
use Drupal\sm_scheduler_test\ScheduledTestMessage;
use Symfony\Component\Clock\Clock;
use Symfony\Component\DependencyInjection\ServiceLocator;

/**
 * Tests Symfony Messenger Scheduler.
 */
final class SmSchedulerSchedulerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'sm_scheduler_test',
    'sm_scheduler',
    'sm',
  ];

  /**
   * Tests scheduler.
   *
   * Implements a simple version of \Symfony\Component\Messenger\Worker.
   *
   * @covers \Symfony\Component\Scheduler\Messenger\SchedulerTransport
   *
   * @see \Drupal\sm_scheduler_test\Messenger\ScheduledTestMessageScheduleProvider
   * @see \Symfony\Component\Messenger\Worker
   */
  public function testSchedulerTransport(): void {
    $scheduleProviderId = 'scheduler_test';

    /** @var \Symfony\Component\Scheduler\Messenger\SchedulerTransport $schedulerTransport */
    $schedulerTransport = $this->receiverLocator()->get('scheduler_' . $scheduleProviderId);

    /** @var \Symfony\Component\Messenger\RoutableMessageBus $bus */
    $bus = \Drupal::service('test_routable_message_bus');

    $clock = new Clock();
    $end = $clock->now()->modify('+11 seconds');

    /** @var \Symfony\Component\Messenger\Envelope[] $all */
    $all = [];
    while ($end > $clock->now()) {
      $envelopes = $schedulerTransport->get();
      foreach ($envelopes as $envelope) {
        $all[] = $bus->dispatch($envelope);
      }

      $clock->sleep(0.250);
    }

    // The ScheduledTestMessageScheduleProvider schedule provider emits
    // messages every 2 seconds. Over a duration of 11 seconds, this results
    // in 5 messages.
    static::assertCount(5, $all);
    for ($i = 0; $i < 5; $i++) {
      $message = $all[$i]->getMessage();
      $this->assertInstanceOf(ScheduledTestMessage::class, $message);
      static::assertEquals(ScheduledTestMessageHandler::class . '::__invoke', $all[$i]->getMessage()->handledBy);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    parent::register($container);

    // We just need to access the service for testing only, so alias it.
    $container
      ->setAlias('test_routable_message_bus', 'messenger.routable_message_bus')
      ->setPublic(TRUE);
  }

  /**
   * The receiver (transport) locator.
   */
  private function receiverLocator(): ServiceLocator {
    return \Drupal::service('messenger.receiver_locator');
  }

}
