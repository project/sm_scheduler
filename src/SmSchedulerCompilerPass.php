<?php

declare(strict_types = 1);

namespace Drupal\sm_scheduler;

use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\Scheduler\Attribute\AsSchedule;

/**
 * Service provider for Symfony Messenger Scheduler.
 */
final class SmSchedulerCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    // Creates services for message handlers.
    foreach ($this->getMessengerHandlerClasses($container->getParameter('container.namespaces')) as $className) {
      // Don't create a service definition if this class is already a service.
      if ($container->hasDefinition($className)) {
        continue;
      }

      // registerAttributeForAutoconfiguration requires services to be created
      // beforehand. Symfony normally creates services for everything in src/.
      $definition = new Definition($className);
      $definition
        // Note: Autoconfigure does not work in services.yml file in < Drupal 11
        // See https://www.drupal.org/project/drupal/issues/3221128
        ->setAutoconfigured(TRUE)
        ->setAutowired(TRUE)
        ->setPublic(FALSE);

      $anonymousHash = ContainerBuilder::hash($className . mt_rand());
      $container->setDefinition('.' . $anonymousHash, $definition);
    }

    // Registers classes/methods with AsSchedule as a schedule provider.
    // Pulled from Symfony' FrameworkExtension.
    $container->registerAttributeForAutoconfiguration(AsSchedule::class, static function (ChildDefinition $definition, AsSchedule $attribute): void {
      $definition->addTag('scheduler.schedule_provider', ['name' => $attribute->name]);
    });
  }

  /**
   * Get MessengerHandler classes for the provided namespaces.
   *
   * @param array<class-string, string> $namespaces
   *   An array of namespaces. Where keys are class strings and values are
   *   paths.
   *
   * @return \Generator<class-string>
   *   Generates class strings.
   *
   * @throws \ReflectionException
   */
  private function getMessengerHandlerClasses(array $namespaces): \Generator {
    foreach ($namespaces as $namespace => $dirs) {
      $dirs = (array) $dirs;
      foreach ($dirs as $dir) {
        $dir .= '/Messenger';
        if (!file_exists($dir)) {
          continue;
        }
        $namespace .= '\\Messenger';

        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_FILEINFO | \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);
        foreach ($iterator as $fileinfo) {
          if ($fileinfo->getExtension() !== 'php') {
            continue;
          }

          /** @var \RecursiveDirectoryIterator|null $subDir */
          $subDir = $iterator->getSubIterator();
          if (NULL === $subDir) {
            continue;
          }

          $subDir = $subDir->getSubPath();
          $subDir = $subDir ? str_replace(DIRECTORY_SEPARATOR, '\\', $subDir) . '\\' : '';
          $class = $namespace . '\\' . $subDir . $fileinfo->getBasename('.php');

          $reflectionClass = new \ReflectionClass($class);

          if (count($reflectionClass->getAttributes(AsSchedule::class)) > 0) {
            yield $class;
          }
        }
      }
    }
  }

}
