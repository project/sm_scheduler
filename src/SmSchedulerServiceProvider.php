<?php

declare(strict_types = 1);

namespace Drupal\sm_scheduler;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\Argument\ServiceLocatorArgument;
use Symfony\Component\DependencyInjection\Argument\TaggedIteratorArgument;
use Symfony\Component\Scheduler\DependencyInjection\AddScheduleMessengerPass;

/**
 * Service provider for Symfony Messenger Scheduler.
 */
final class SmSchedulerServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $container
      // Priority 200 to execute before AttributeAutoconfigurationPass which has
      // priority 100. See symfony/dependency-injection/Compiler/PassConfig.php.
      ->addCompilerPass(new SmSchedulerCompilerPass(), priority: 200)
      // This must execute before
      // \Drupal\symfony_messenger\SymfonyMessengerCompilerPass which has a
      // priority of 0.
      // It must also run before
      // SymfonyMessengerUnprivatizeScheduleProviderCompilerPass due to
      // https://www.drupal.org/project/drupal/issues/3108020
      ->addCompilerPass(new AddScheduleMessengerPass(), priority: 100)
      ->addCompilerPass(new SmSchedulerUnprivatizeScheduleProviderCompilerPass());

    $container
      ->getDefinition('sm_scheduler.transport_factory')
      ->setArgument(0, new ServiceLocatorArgument(new TaggedIteratorArgument('scheduler.schedule_provider', 'name')));
  }

}
